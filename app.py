from __future__ import division
from flask import Flask,render_template,Response,session,request,redirect,url_for,jsonify
import web,requests
from flask.ext.pymongo import PyMongo
import json
import os
import fileinput


app = Flask(__name__)
app.secret_key = 'Fuckedupworld'

API_KEY="aZAYYsh9c9nRuwOSlJCzJ3uRJIBDoLGW"

mongo = PyMongo(app)
#Just Some Ping Tests
@app.route('/ping')
def ping():
    r=requests.get('http://terminal2.expedia.com/x/mhotels/search?city=GOA&checkInDate=2016-12-01&checkOutDate=2016-12-03&room=5&apikey=PkUgDzws2rbe8oLyGQAhCw7I6IJnxoMB')
    mongo.db.tests.insert(r.json())
    thc = r.json()['totalHotelCount']
    ahc = r.json()['availableHotelCount']
    pc = (ahc/thc)*100
    print pc
    return "<html><head><script type='text/javascript'>console.log("+r.text+");</script></head></html>"

def dic_cons():
    dc={}
    return dc

@app.route('/',methods=['GET','POST'])
def home():
    return render_template('index.html',name="Saurabh Badhwar")

@app.route('/crowd',methods=['GET'])
def crowd():
    if 'city' in request.args and 'cid' in request.args and 'cod' in request.args:
        payload="http://terminal2.expedia.com/x/mhotels/search?city="+request.args.get('city')+"&checkInDate="+request.args.get('cid')+"&checkOutDate="+request.args.get('cod')+"&room=2&apikey="+API_KEY
        data=requests.get(payload)
        try:
            thc = data.json()['totalHotelCount']
            ahc = data.json()['availableHotelCount']
            pc = (ahc/thc)*100
        except KeyError:
            pc = 51
        if pc<=15:
            status=2
        elif pc<=50:
            status=1
        else:
            status=0
    return jsonify(crowd=status,city=request.args.get('city'),pci=pc)

@app.route('/path', methods=['GET','POST'])
def path():
    dic={}
    dc={}
    dic['vehicles']=[{"vehicle_id":"innova","start_address":{'location_id':"",'lon': 0.0,'lat': 0.1}}]
    dic['services']=[]
    if 'city' in request.args:
        cities=request.args.get('city').split(',')
        cities.reverse()
        dic['vehicles'][0]["start_address"]=getGeo(cities.pop())
        print cities
        for i in cities:
            print i
            dc["id"]=i
            dc["name"]=i      
            dc["address"]=getGeo(i)
            dic['services'].append(dc.copy())
            dc.clear()
        jdic=json.dumps(dic)
        req1="https://graphhopper.com/api/1/vrp/optimize?key=d05f4946-6525-4bbb-8260-b143e196c57d"
        r=requests.post(req1,data=jdic)
        print r.text
        rjson=r.json()
        job_id=rjson["job_id"]
        #print job_id
        req2="https://graphhopper.com/api/1/vrp/solution/"+job_id+"?key=d05f4946-6525-4bbb-8260-b143e196c57d"
        #r2=requests.get(req2)
        while True:
            r2=requests.get(req2)
            fnl=r2.json()
            if fnl['solution']!="pending":
                break
        a=fnl['solution']
        b=a['routes'][0]
        c=b['activities']
        e=[]
        for d in c:
            e.append(d['location_id'])
    return jsonify(e)
        
        
 
def getGeo(city):
    data=mongo.db['geocode'].find_one({"city":city})
    geo={}
    geo["location_id"]=data['city']
    geo["lat"]=data["lat"]
    geo["lon"]=data["lon"]
    return geo

if __name__ == "__main__":
	app.run(debug=True)
